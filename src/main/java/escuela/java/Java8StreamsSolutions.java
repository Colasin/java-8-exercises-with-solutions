package escuela.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class Java8StreamsSolutions {

    protected static List<Person> sortStudentsByFirstName(List<Person> people) {
        return people.stream().sorted((p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName())).collect(Collectors.toList());
    }

    public static List<Person> filterPeopleBetween20And25(List<Person> people) {
        return people.stream().filter(p -> p.getAge().compareTo(20) >= 0 && p.getAge().compareTo(25) <= 0).collect(Collectors.toList());
    }

    public static List<Person> filterPeopleWithNameStartedWithP(List<Person> people) {
        return people.stream().filter(p -> p.getFirstName().startsWith("P")).collect(Collectors.toList());
    }

    public static boolean isThereSomeoneOlderThan35(List<Person> people) {
        return people.stream().anyMatch(p -> p.getAge().compareTo(35) > 0);
    }

    public static double calculateAgeAverage(List<Person> people) {
        return  people.stream()
                //.map(Person::getAge)
                .mapToDouble(Person::getAge)
                .average()
                .getAsDouble();
        //Integer ageSum = people.stream().map(Person::getAge).reduce(0, (p1, p2) -> p1 + p2);
        //return (float) ageSum/people.size();
    }
}
